﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace 参展大屏幕
{
	public partial class carb : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				try
				{
					Random r = new Random(Guid.NewGuid().GetHashCode());
					int i = r.Next(0, 3);
					string name = "";
					switch (i)
					{
						case 0:
							name = "环固城湖水域";
							break;
						case 1:
							name = "环石臼湖水域";
							break;
						case 2:
							name = "环水阳江水域";
							break;
					}
					ViewState["name"] = name;
					StatList.SetOne(i);
				}
				catch
				{
				}
			}
		}
	}
}