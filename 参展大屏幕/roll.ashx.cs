﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 参展大屏幕
{
	/// <summary>
	/// roll 的摘要说明
	/// </summary>
	public class roll : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";

			if (StatList.Count > 0)
			{
				int i = StatList.GetOne();
				context.Response.Write("{\"stat\":\"ok\",\"value\":\"" + i + "\"}");

			}
			else
			{
				context.Response.Write("{\"stat\":\"empty\"}");
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}