﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 参展大屏幕
{
	public static class StatList
	{
		public static Queue<int> sList = new Queue<int>();
		private static object obj = new object();
		/// <summary>
		/// 从队列中取出一个
		/// </summary>
		/// <returns></returns>
		public static int GetOne()
		{
			lock (obj)
			{
				return sList.Dequeue();
			}

		}

		/// <summary>
		/// 塞一个对象进队列
		/// </summary>
		/// <param name="i"></param>
		public static void SetOne(int i)
		{
			lock (obj)
			{
				sList.Enqueue(i);
			}
		}

		/// <summary>
		/// 返回所有
		/// </summary>
		/// <returns></returns>
		public static Queue<int> getAll()
		{
			lock (obj)
			{
				return sList;
			}
		}

		/// <summary>
		/// 当前队列的长度
		/// </summary>
		public static int Count
		{
			get { lock (obj) { return sList.Count; } }
		}
	}
}