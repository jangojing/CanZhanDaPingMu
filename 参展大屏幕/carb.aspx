﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="carb.aspx.cs" Inherits="参展大屏幕.carb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- 优先使用 IE 最新版本和 Chrome -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<!-- 是否启用 WebApp 全屏模式，删除苹果默认的工具栏和菜单栏 -->
	<meta name="apple-mobile-app-capable" content="yes">
	<!-- 改变顶部状态条的颜色 -->
	<meta name="apple-mobile-app-status-bar-style" content="black">
	<!-- 页面描述 -->
	<meta name="description" content="">
	<!-- 页面关键词 -->
	<meta name="keywords" content="固城湖螃蟹大数据服务中心">
	<!-- 忽略页面中的数字识别为电话，忽略email识别 -->
	<meta name="format-detection" content="telphone=no, email=no" />
	<!-- 为移动设备添加 viewport -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<title>固城湖螃蟹大数据服务中心</title>
	<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
	<!-- 包括所有已编译的插件 -->
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/echarts.common.min.js" />
	<script src="http://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
		crossorigin="anonymous"></script>
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<script src="https://cdn.bootcss.com/echarts/3.7.0/echarts.common.js"></script>
	<script src="https://cdn.bootcss.com/echarts/3.7.0/echarts.js"></script>
	<style type="text/css">
		.long
		{
			width: 100%;
			padding-bottom: 10px;
			margin: 0 auto;
			background-color: #062540;
		}
		.head
		{
			width: 100%;
			height: 60px;
			background-color: #011127;
			margin-bottom: 15px;
		}
		.h_left
		{
			width: 50%;
			height: 60px;
			float: left;
			line-height: 60px;
			text-align: center;
			padding-top: 10px;
			border-right: 1px solid #273548;
			color: #4D9CAB;
			text-align: center;
		}
		.h_right
		{
			width: 50%;
			height: 60px;
			float: left;
			padding-top: 10px;
			text-align: center;
			line-height: 60px;
			color: #4D9CAB;
			border-right: 1px solid #273548;
		}
		.act
		{
			color: #ffffff;
			border-bottom: 6px solid #A9CFD6;
		}
		
		.foot
		{
			width: 204px;
			height: 57px;
			background-color: #D74A04;
			text-align: center;
			line-height: 57px;
			margin: 0 auto;
			border-radius: 5px;
		}
		.foot a
		{
			display: block;
			width: 100%;
			height: 100%;
			padding-right: 20px;
			font-weight: bolder;
			background: url(./css/img/2.png) no-repeat;
			background-position-x: 130px;
		}
		
		a
		{
			color: #ffffff;
			cursor: pointer;
			text-decoration: none;
		}
		a:link
		{
			color: #ffffff;
			cursor: pointer;
			text-decoration: none;
		}
		/* 未被访问的链接 */
		a:visited
		{
			color: #ffffff;
			cursor: pointer;
			text-decoration: none;
		}
		/* 已被访问的链接 */
		a:hover
		{
			color: #ffffff;
			cursor: pointer;
			text-decoration: none;
		}
		/* 鼠标指针移动到链接上 */
		a:active
		{
			color: #ffffff;
			cursor: pointer;
			text-decoration: none;
		}
		/* 正在被点击的链接 */
		
		.content
		{
			margin-bottom: 10px;
		}
		table
		{
			width: 100%;
			margin: 0 auto;
			padding: 0 10px;
			color: #FFFFFF;
		}
		tr
		{
			border-bottom: 1px solid #273548;
			height: 40px;
		}
		.table_h
		{
			width: 100%;
		}
		
		.th1
		{
			height: 54px;
			background-color: #20405B;
			line-height: 54px;
		}
		.td1
		{
			padding-left: 14px;
		}
		.td_mid
		{
			font-size: small;
			color: #A8A8A8;
		}
		.td_right
		{
			font-size: small;
			color: #FFFF00;
		}
		table img
		{
			width: 15px;
			height: 15px;
		}
		
		
		
		
		
		
		ul
		{
			list-style: none;
			padding: 0 10px;
			margin: 0 auto;
			color: #A0A0A0;
			font-size: 16px;
		}
		li
		{
			padding-bottom: 10px;
			padding-top: 10px;
			padding-left: 14px;
		}
		.line
		{
			border-bottom: 1px solid #273548;
		}
		.r_right
		{
			float: right;
			color: #ffffff;
			padding-right: 14px;
		}
		.num
		{
			color: #FF6600;
			font-size: 20px;
		}
		.img
		{
			float: right;
			color: #ffffff;
			position: relative;
			top: -10px;
			padding-right: 14px;
		}
		.img img
		{
			position: relative;
			top: -5px;
		}
		.myhead
		{
			height: 40px;
			line-height: 40px;
			color: #fff;
			background-color: #011127;
			text-align: center;
			font-weight: bolder;
			border-bottom: 1px solid #A9CFD6;
		}
		.myfooter
		{
			background: #011127;
			line-height: 30px;
			text-align: center;
			color: #A0A0A0;
		}
	</style>
</head>
<body>
	<div class="myhead">
		追溯序列号：2017875478002
	</div>
	<div id="eat" class="long">
		<div class="head">
			<div class="h_left act">
				<h4>
					饵料结构</h4>
			</div>
			<div class="h_right">
				<h4>
					质检报告</h4>
			</div>
		</div>
		<div class="content">
			<div id="main" style="height: 300px; width: 100%; margin: 0 auto">
			</div>
			<script type="text/javascript">
				var myChart = echarts.init(document.getElementById('main'));
				var legendName = { benxietang: '2017Z001塘口', avg: '高淳区平均水平' };
				var option = {
					color: ['#ccc', 'red'],
					legend: {
						right: '0%',
						top: '-5px',
						orient: 'vertical',
						data: [
							{
								name: legendName.benxietang,
								textStyle: {
									color: '#fff'
								}
							}, {
								name: legendName.avg,
								textStyle: {
									color: '#fff'
								}
							}
						]

					},
					radar: [
						{
							indicator: [
								{ text: '活螺丝',max:1050 },
								{ text: '小麦',max:200 },
								{ text: '颗粒饲料',max:100 },
								{ text: '玉米',max:120 },
								{ text: '冰鲜鱼',max:600 }
							],

							radius: 120,
							startAngle: 90,
							splitNumber: 4,
							shape: 'circle',
							name: {
								formatter: '{value}',
								textStyle: {
									color: 'white'
								}
							},
							splitArea: {
								areaStyle: {
									color: [
										'rgba(0,0,0,0)',
										'rgba(0,0,0,0)', 'rgba(0,0,0,0)',
										'rgba(0,0,0,0)', 'rgba(0,0,0,0)'
									],
									shadowColor: 'rgba(0, 0, 0, 0.3)',
									shadowBlur: 10
								}
							},
							axisLine: {
								lineStyle: {
									color: 'rgba(255, 255, 255, 0.5)'
								}
							},
							splitLine: {
								lineStyle: {
									color: 'rgba(255, 255, 255, 0.5)'
								}
							}
						}
					],
					series: [
						{
							name: '雷达图',
							type: 'radar',
							itemStyle: {
								emphasis: {
									// color: 各异,
									lineStyle: {
										width: 4
									}
								}
							},
							data: [
						
								{
									value: [750, 100, 50, 105, 400],
									name: legendName.avg,
									itemStyle: {
										normal: {
											lineStyle: { color: "#ccc" }
										}
									},
									
									areaStyle: {
										normal: {
											color: 'rgba(255, 255, 255, 0.5)'
										}
									}
								}, {
									value: [1000, 0, 30, 100, 550],
									name: legendName.benxietang,
									itemStyle: {
										normal: {
											lineStyle: { color: "red" }
										}
									},
								
									symbol: 'rect',
									symbolSize: 2,
									areaStyle: {
										normal: {
										  color: 'rgba(1,126,177, 0.5)',

										}
									}

								},
							]
						}
					]
				};
				myChart.setOption(option);
				window.onresize = myChart.resize;
			</script>
			<ul>
				<li class="line">螃蟹规格<span class="r_right"><b class="num">4.0 </b>两</span></li>
				<li class="line">养殖水域<span class="r_right"><%= ViewState["name"]%></span></li>
				<li>螃蟹分级<span class="img"><img src="css/img/1.png" width="40" height="40" />金蟹</span></li>
			</ul>
		</div>
		<div class="foot">
			<a href="http://weidian.com/s/1238325582?ifr=shopdetail&wfr=c">点击购买</a>
		</div>
	</div>
	<div id="qos" class="long" style="display: none;">
		<div class="head">
			<div class="h_left ">
				<h4>
					饵料结构</h4>
			</div>
			<div class="h_right act">
				<h4>
					质检报告</h4>
			</div>
		</div>
		<div class="content">
			<div class="table_h">
				<table>
					<tr class="th1">
						<th class="td1">
							检验项目
						</th>
						<th>
							技术要求
						</th>
						<th>
							实测结果
						</th>
						<th>
						</th>
					</tr>
					<tr>
						<td class="td1">
							铅（以Pb计）
						</td>
						<td class="td_mid">
							≤0.5mg/kg
						</td>
						<td class="td_right">
							0.1mg/kg
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							镉（以Cb计）
						</td>
						<td class="td_mid">
							≤0.5mg/kg
						</td>
						<td class="td_right">
							0.04mg/kg
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							六六六
						</td>
						<td class="td_mid">
							≤0.5mg/kg
						</td>
						<td class="td_right">
							未测出
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							滴滴滴
						</td>
						<td class="td_mid">
							≤0.5mg/kg
						</td>
						<td class="td_right">
							未测出
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							氯霉素
						</td>
						<td class="td_mid">
							不得检出
						</td>
						<td class="td_right">
							未测出
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							孔雀石绿机器代谢物
						</td>
						<td class="td_mid">
							不得检出
						</td>
						<td class="td_right">
							未测出
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							AOZ
						</td>
						<td class="td_mid">
							不得检出
						</td>
						<td class="td_right">
							未测出
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
					<tr>
						<td class="td1">
							AMOZ
						</td>
						<td class="td_mid">
							不得检出
						</td>
						<td class="td_right">
							未测出
						</td>
						<td>
							<img src="css/img/3.png">
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="foot">
			<a href="http://weidian.com/s/1238325582?ifr=shopdetail&wfr=c">点击购买</a>
		</div>
	</div>
	<div class="myfooter">
		实施单位：南京良庄农产品有限公司<br />
		数据支撑：固城湖螃蟹大数据服务中心
	</div>
</body>
</html>
<script>
	$().ready(function () {
		console.log($(window).height());
		if ($(".long").height() < $(window).height() - 100) {
			$(".long").height($(window).height() - 100);

		}
		$(".h_left").click(function () {
			$(".long").hide();
			$("#eat").show();
			if ($(".long").height() < $(window).height() - 100) {
				$(".long").height($(window).height() - 100);

			}
		});
		$(".h_right").click(function () {
			$(".long").hide();
			$("#qos").show();
			if ($(".long").height() < $(window).height() - 100) {
				$(".long").height($(window).height() - 100);

			}
		});

	});


</script>
